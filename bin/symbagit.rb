#!/usr/local/bin/ruby

module BagIt
  class Bag
    ##
    # Allows add_file to use symbolic links for files, so there's no need to
    # copy content.
    def add_file(relative_path, src_path = nil, symbolic = false)
      path = File.join(data_dir, relative_path)
      raise "Bag file exists: #{relative_path}" if File.exist? path
      FileUtils.mkdir_p File.dirname(path)

      f = if src_path.nil?
            File.open(path, "w") { |io| yield io }
          else
            if symbolic
              File.symlink(src_path, path)
            else FileUtils.cp src_path, path
            end
          end
      write_bag_info
      f
    end

    ##
    # Use SHA256 for tagmanifest files. Chronopolis ingest expects this.
    def add_tag_file(path, src_path = nil)
      f = File.join(@bag_dir, path)
      raise "Tag file already in manifest: #{path}" if tag_files.include?(f)

      if !File.exist? f
        FileUtils.mkdir_p File.dirname(f)

        # write file
        if src_path.nil?
          File.open(f, "w") { |io| yield io }
        else
          FileUtils.cp src_path, f
        end
        # this adds the manifest and bag info files on initial creation
        # it must only run when the manifest doesn't already exist or it will
        # infinitely recall add_tag_file. Better way of doing this?
        tagmanifest!
      elsif !src_path.nil?
        raise "Tag file already exists, will not overwrite: #{path}\n Use add_tag_file(path) to add an existing tag file."
      end

      data = File.open(f, &:read)
      rel_path = Pathname.new(f).relative_path_from(Pathname.new(bag_dir)).to_s

      # sha256
      File.open(tagmanifest_file(:sha256), "a") do |io|
        io.puts "#{Digest::SHA256.hexdigest(data)} #{rel_path}"
      end

      tag_files
    end
  end
end
