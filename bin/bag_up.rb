#!/usr/local/bin/ruby

require 'bagit'
require_relative 'symbagit'

# Dir.mkdir '/tmp/bags'

def bag_file(file)
  shortname = File.basename(file)
  puts("shortname: #{shortname}")

  Dir.mkdir("/tmp/bags/#{shortname}")

  bag = BagIt::Bag.new("/tmp/bags/#{shortname}")
  bag.add_file(shortname, "#{file}", true)
  bag.manifest!(algo: 'sha256')

  puts("uh oh, #{file} made an invalid bag") unless bag.valid?
end

content_path = '/mnt/special'
Dir.entries(content_path).each do |file|
  next if ['.', '..'].include?(file)

  if File.directory?(File.join(content_path,file))
    cur_dir = File.join(content_path,file)
    Dir.entries(cur_dir).each do |content|
      next if ['.', '..'].include?(content)

      cur_file = File.join(cur_dir,content)
      if File.file?(cur_file)
        bag_file(cur_file)
      else raise "oops, #{cur_file} is a nested subdirectory"
      end
    end
  elsif File.file?(file)
    bag_file(file)
  else raise "wow, #{file} is not a directory or file"
  end
end
