FROM ruby:2.7

RUN mkdir /data
WORKDIR /data

COPY ./Gemfile /data
RUN bundle

COPY ./bin /data/bin

CMD bin/bag_up.rb