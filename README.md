UCSB Chronopolis Pilot
======================

Tools developed during UCSB's chronopolis preservation pilot.

The goal of this work is to bag up some UCSB content and push it into
Chronopolis.

## Run this:

```sh
docker build . -t pilot
docker run --mount type=bind,source=(pwd)/test-content,destination=/mnt/special,readonly --mount type=bind,source=(pwd)/bags,destination=/tmp/bags -it pilot
```

This leaves a bag for each file in `./test-content` in `./bags`.
