# UCSB Chronopolis Pilot

## Executive Summary

UC Santa Barbara and UC San Diego libraries collaborated on a small-scale pilot testing the
use of Chronopolis as a digital preservation service with a set of at-risk digitized audio
files. The goal of this pilot was to determine the feasibility of an internal University of
California process for preserving content in Chronopolis.

The two campuses were successful in ingesting 7.7 TB of digitized audio and related
files for preservation. These files represented 37,520 distinct objects.

There is room for improvement both in the technology and in the handling and preparation of
content for preservation, but ultimately the approach used would allow UCSB to recover from
data loss in the pilot collection.

### Process

UC Santa Barbara identified approximately 7.7 TB of digitized 78 rpm disks as the pilot
content. These materials are being actively digitized onto a network drive. At the time
of the pilot there were 37,520 objects included in this set.

For the purposes of the pilot, we tested a process to deposit the content to Chronopolis
_en masse_. UC Santa Barbara packaged the content files as [BagIt][bagit] "bags", checked
each object's bag for validity, then synchronized the resulting directory structure and
files to an ingest server at UC San Diego. The "bag" structure serves a dual purpose: to
bundle together files related to a single object (a 78 rpm disk) and provide a manifest
(ensuring "completeness"), and to guarantee bit-level fidelity ("validity") when transferring
files by including checksums for each file. Once synchronized, the objects were ingested
into Chronopolis' test nodes.

This process closely aligns with UC San Diego's internal deposit process. Synchronizing
Santa Barbara's bags directly to San Diego's ingest server avoids the need to deposit
through Lyrasis' (formerly Duraspace's) `DuraCloud` commercial offering.

### Infrastructure

The process used for this pilot involved two systems:

  - A virtual machine on premises at UC Santa Barbara; and
  - UC San Diego's existing Chronopolis ingest system.

The UC Santa Barbara system needed be on premises in order to mount the network drive
containing the digitized content. The drive was mounted as read-only to avoid risk of
changes to content files during this work. The system also needed a `docker`
installation to support the BagIt tooling described below, and `rsync` to support file
transfer. Lastly, this system needed a modestly sized secondary disk to store the bag
directory structure, manifests, and tag files. UCSB's IT Operations team provisioned
this system.

The UC San Diego Chronopolis ingest system existed at the start of the pilot. With
the exception of networking (see below), the processes supported by this system were
part of its existing routine use.

#### Networking

These two systems needed an open connection for SSH (port 22), which required intervention
by each campus' operations teams. The UCSD system is behind a firewall with IP based filtering,
so it was necessary to provide a stable outgoing IP for outgoing connections from the UCSB
system. We tried to use the `squid` proxy, which is commonly used for outgoing HTTP connections
from UCSB's network. This didn't work since the proxy isn't configured for SSH connections.
Ultimately, we provisioned the system a stable public IP and opened its ports directly. We would
likely need to reproduce this network arrangement as a more permanent pipeline between the
campuses to continue transferring files in this way.

### Content

The drive mounted to the UC Santa Barbara system contains 4 directories:

  - `78rpm/avlab`
  - `78rpm/repo`
  - `78rpm/repo alias`
  - `78rpm/TestFiles`

This pilot worked only with the `78rpm/repo` directory, which contains 37,520
sub-directories. For the purposes of the pilot, we interpreted these sub-directories
as corresponding directly with "objects" in the collection and, therefore, bags pushed
to Chronopolis.

The primary form of metadata available in this process was the directory/file names.
By convention, both the files within a given directory and the directory itself share
a name identifying the object. For example, `cusb_victor_35027_01_c6482_02` contains:

```txt
  - cusb_victor_35027_01_c6482_02d.mp3
  - cusb_victor_35027_01_c6482_02d.mp3..old
  - cusb_victor_35027_01_c6482_02d.mp3.old2
  - cusb_victor_35027_01_c6482_02.jpg
  - cusb_victor_35027_01_c6482_02m.wav
  - cusb_victor_35027_01_c6482_02.tiff
  - cusb_victor_35027_01_c6482_02.wav
```

While we can infer much about the content from these names we chose for the pilot to
treat all files equally (as preservation content). This includes (as in the example above)
some objects which appear to be derivative of an archival master, and some which bear
extensions that are variations on `.old`. Some directories also contained one or more
`.md5` files and/or a `.DS_Store`. We treated these as preservation content as well.

These assumptions are probably **not** ones we want to make when genuinely preserving this
collection or others, but have the following advantages:

  - In the event of data loss it would be possible to completely reconstruct `78rpm/repo`
    from content recovered from Chronopolis;
  - It required little depth of understanding of the collection's content and variations
    in digitization practices, expediting the pilot.

The lack of further metadata has an impact on the quality of expected preservation outcomes
attributable to the pilot process, but we considered metadata from sources other than
the `78rpm/repo` directory out-of-scope for this pilot.

### Software

To support generating bags for the 78 rpm files, UCSB's Digital Library Development and
IT Operations team developed a [simple script][bag-up-rb] leveraging the
[Ruby BagIt][bagit-rb] library. This script loops over the sub-directories in 78 rpm
`repo` path, creating a bag for each directory. All included files are treated as content (see above).
As a result the bagged and preserved content may be substantially duplicative. We didn't attempt
to validate files against any checksums when they existed), or verify that file names matched the
directory name before proceeding with bagging and preservation.

This script was purpose built to support the pilot context and its re-usability is limited.

However, it does support a notable feature that can be considered a useful
of the pilot: the bag creation process involves neither copying the content files, nor changing
anything on the original source disk. Instead, bags are created in a new directory and
contents are included via symlink. This reduces the disk overhead for the bags
content from about 7.7 TB (a complete copy) to approximately 1 GB (manifests,
bag metadata, and tag files) for the pilot content. It also reduces the time involved
substantially, by avoiding writing large amounts of content to disk. (Special thanks to
Michael Craig for implementing this feature).

We also had to extend the Ruby bag library to support `SHA256` checksums for tag
files. Chronopolis does not accept the library's default (`MD5` and `SHA1` checksum algorithms),
requiring the more secure `SHA256`. Ruby's library already supported `SHA256` for "payload"
checksums, but we needed to extend it to provide them for tag files.

The script runs as a single process, handling one object/bag at a time. Its overall runtime
on the Santa Barbara system was about 3 days to create bags for 37,520 objects. The script is
equipped to quickly resume a partially complete bagging process. As a result, it can be run
additively when new objects are digitized. However, it's not capable of detecting changes to
existing content or updating bags created in a previous run.

The script is included in a container which packages its system dependencies, which makes it
portable across systems. During the pilot, we used this to facilitate local development and
testing.

There are many optimizations to this process that may be worth exploring if we choose to
pursue this approach for a larger set of content. We made the choice not to refine this long
running, purpose built process to reduce the work needed to achieve the objectives of the
pilot.

The `rsync` synchronization is also a multi-day process, dependent primarily on the bandwidth
connection between the systems. The two processes can be run in tandem, so the total runtime
to complete the process for this collection wasn't much more than the 3 days required to create
the bags.

### Outcomes & Other Considerations

The primary outcome of this project was the successful demonstration of a UC internal process for
depositing content into Chronopolis. The technical infrastructure supporting this process
is relatively simple and, while the process itself is somewhat time and resource intensive, there
are many opportunities to optimize for preservation at a larger scale.

As a side effect, the process produced transparent,  small-footprint BagIt bags for the pilot content.
These bags have added value for the collection beyond the pilot scope:

  - The generated manifests and `SHA256` checksums are an improvement over the existing
    `filename.wav.md5` convention for ensuring completeness and detecting change on the
    local directory;
  - We could choose to export existing metadata to these bags, bundle content and metadata
    without changing the existing file layouts or digitization workflows;
  - The BagIt standard represents an attractive ingest format for Surfliner/Comet, so
    the work involved in preparing this content for preservation may pay off in other
    contexts.

Plans to adopt this approach for preservation of similar UC Santa Barbara collections should take
into account the collection-specific approach used for this content. There is a need for technical
and process improvements to support scale, cost-effectiveness, and quality of preserved content, but
also opportunity for UCSB's collections to benefit broadly from work to support preservation.

[bagit]: https://tools.ietf.org/html/rfc8493
[bagit-rb]: https://rubygems.org/gems/bagit/
[bag-up-rb]: https://gitlab.com/ucsb-library/chronopolis-pilot/
